import React, { useEffect } from "react";
import { HashRouter as Router, Routes, Route } from "react-router-dom";
import LoginComponent from "../containers/Login";
import ResetPassword from "../containers/ResetPassword";
import Dashboard from "../containers/Dashboard";
import Profile from "../containers/Profile";
import Users from "../containers/Users";
import Places from "../containers/Places";

import { connect } from "react-redux";
//import { logOut } from "../actions/User";

const RouteConfig = (props) => {
  const { isLogin, onlogOut } = props;
  useEffect(() => {
    if (!isLogin) {
    }
  }, [isLogin, onlogOut]);

  return (
    <>
      <Router hashType="slash">
        <Routes>
          <Route exact path="/" element={<LoginComponent />} />
          <Route exact path="/login" element={<LoginComponent />} />
          <Route exact path="/forgotPassword" element={<ResetPassword />} />
          <Route exact path="/dashboard" element={<Dashboard />} />
          <Route exact path="/profile" element={<Profile />} />
          <Route exact path="/user" element={<Users />} />
          <Route exact path="/places" element={<Places />} />

          <Route path="*" element={<LoginComponent />} />
        </Routes>
      </Router>
    </>
  );
};

const mapStateToProps = (state) => ({
  isLogin: state?.userInfo?.loginStatus ?? false,
});

const mapDispatchToProps = (dispatch) => ({
  // onlogOut: () => dispatch(logOut()),
});
export default connect(mapStateToProps, mapDispatchToProps)(RouteConfig);
