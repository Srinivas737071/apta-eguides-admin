import React from "react";
import Layout from "../../components/layout";
import PlacesListTable from "../../components/placesListTable";
import Breadcrumb from "../../components/breadcrumb";
import "./places.scss";
import { Row, Col, Button } from "react-bootstrap";


const Places = () => {
  return (
    <Layout
      tabActive="places"
      innerComponent={() => {
        return (
          <div className="placesContainer">
            <Row>
              <Col xs={12} md={8} lg={8}>
                <Breadcrumb
                  title={"Places"}
                  data={[
                    {
                      name: "Home",
                      link: "/dashboard",
                    },
                    {
                      name: "Places",
                      link: "",
                    },
                  ]}
                />
              </Col>
              <Col xs={12} md={4} lg={4} className="buttonContriner">
                <Button className="addButton btn-info btn-lg btn-block">
                  Add Place
                </Button>
              </Col>
            </Row>

            <PlacesListTable />
          </div>
        );
      }}
    />
  );
};

export default Places;
