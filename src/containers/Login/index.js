import React, { useState, useEffect } from "react";
import "./login.scss";
import { connect } from "react-redux";
import {
  Container,
  Row,
  Col,
  Image,
  Form,
  InputGroup,
  Button,
} from "react-bootstrap";
import empty from "is-empty";
import { getCaptcha, userLogin } from "../../actions/User";
import { useNavigate } from "react-router-dom";
import { LOGIN_STATUS_SUCCESS } from "../../actions/types";

const Login = (props) => {
  const { onLoginSucces } = props;
  const history = useNavigate();

  const [imageCaptcha, setimageCaptcha] = useState("");
  const [captchaData, setCaptchaData] = useState({});
  const [form, setForm] = useState({});
  const [errors, setErrors] = useState({});
  const [validated, setValidated] = useState(null);

  const loadCaptcha = async () => {
    setimageCaptcha("");
    setCaptchaData({});
    const response = await getCaptcha();
    if (response && response.data) {
      setCaptchaData(response.data);
      setimageCaptcha(
        "https://stage-admin1.aptaeguides.com" + response.data.image_url
      );
    }
  };

  const restData = () => {
    setForm({});
    const newErrors = {};
    newErrors.username = true;
    newErrors.password = true;
    newErrors.captcha_1 = "Invalid captcha";
    setErrors(newErrors);
    setValidated(false);
    loadCaptcha();
  };

  const postLoginData = async () => {
    let postParames = {
      ...form,
      captcha_0: captchaData.key,
    };
    const response = await userLogin(postParames);
    if (response && response.data) {
      if (response.data.status === true) {
        onLoginSucces(response.data);
        history("/dashboard");
      } else {
        restData();
      }
    } else {
      restData();
    }
  };

  useEffect(() => {
    loadCaptcha();
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value,
    });
    if (!!errors[field])
      setErrors({
        ...errors,
        [field]: null,
      });
  };

  const findFormErrors = () => {
    const { username, password, captcha_1 } = form;
    const newErrors = {};
    if (!username || username === "")
      newErrors.username = "Please enter your username.";
    if (!password || password === "")
      newErrors.password = "Please enter your password.";
    if (!captcha_1 || captcha_1 === "")
      newErrors.captcha_1 = "Please enter captcha.";

    return newErrors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = findFormErrors();
    if (!empty(newErrors)) {
      console.log(newErrors);
      setErrors(newErrors);
      setValidated(false);
    } else {
      postLoginData();
    }
  };

  return (
    <Container
      className="loginContent min-vh-100 d-flex flex-column"
      fluid={true}
    >
      <Row className="loginFormContent">
        <Col xs={12} lg={12} md={12} sm={12}>
          <div className="headerLoginContent">
            <div className="p-1">
              <Image src="../assets/images/apta_logo_new.png" />
            </div>
            <h6 className="card-subtitle line-on-side text-center">
              <span>Login to E-Guides CMS</span>
            </h6>
          </div>
          <div className="loginFromContent">
            <Form validated={validated} onSubmit={handleSubmit}>
              <InputGroup className="mb-4">
                <InputGroup.Text id="basic-addon1">
                  <i className="fa fa-ligthx fa-user"></i>
                </InputGroup.Text>
                <Form.Control
                  placeholder="Your Username"
                  aria-label="Your Username"
                  aria-describedby="basic-addon1"
                  type="text"
                  onChange={(e) => setField("username", e.target.value)}
                  isInvalid={errors.username}
                  value={form?.username ?? ""}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.username}
                </Form.Control.Feedback>
              </InputGroup>

              <InputGroup className="mb-4">
                <InputGroup.Text id="basic-addon1">
                  <i className="fa fa-key"></i>
                </InputGroup.Text>
                <Form.Control
                  placeholder="Password"
                  aria-label="Password"
                  aria-describedby="basic-addon1"
                  type="password"
                  onChange={(e) => setField("password", e.target.value)}
                  isInvalid={errors.password}
                  value={form?.password ?? ""}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.password}
                </Form.Control.Feedback>
              </InputGroup>
              {imageCaptcha && imageCaptcha !== "" && (
                <InputGroup className="mb-4 captcha_img">
                  <InputGroup.Text id="basic-addon1">
                    <Image src={imageCaptcha} />
                  </InputGroup.Text>
                  <Form.Control
                    placeholder="Please enter captcha"
                    aria-label="Please enter captcha"
                    aria-describedby="basic-addon1"
                    type="text"
                    onChange={(e) => setField("captcha_1", e.target.value)}
                    isInvalid={errors.captcha_1}
                    value={form?.captcha_1 ?? ""}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.captcha_1}
                  </Form.Control.Feedback>
                </InputGroup>
              )}

              <Col xs={12} lg={12} md={12} sm={12}>
                <div
                  className="forgotPasswordLink"
                  onClick={() => {
                    history("/forgotPassword");
                  }}
                >
                  Forgot Password?
                </div>
              </Col>

              <Button
                className="loginButton btn-info btn-lg btn-block"
                type="submit"
              >
                <i className="fa fa-thin fa-lock"></i>
                Login
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

const mapDispatchToProps = (dispatch) => ({
  onLoginSucces: (data) => {
    dispatch({
      type: LOGIN_STATUS_SUCCESS,
      payload: data,
    });
  },
});

export default connect(null, mapDispatchToProps)(Login);
