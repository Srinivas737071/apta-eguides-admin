import React, { useState, useEffect } from "react";
import "./resetPassword.scss";
import { connect } from "react-redux";
import {
  Container,
  Row,
  Col,
  Image,
  Form,
  InputGroup,
  Button,
} from "react-bootstrap";
import empty from "is-empty";
import { getCaptcha } from "../../actions/User";
import { useNavigate } from "react-router-dom";

const ResetPassword = (props) => {
  
  const [imageCaptcha, setimageCaptcha] = useState("");
  const navigate = useNavigate();

  const [captchaData, setCaptchaData] = useState({});

  const [form, setForm] = useState({});
  const [errors, setErrors] = useState({});
  const [validated, setValidated] = useState(null);

  const loadCaptcha = async (e) => {
    setimageCaptcha("");
    setCaptchaData({});
    const response = await getCaptcha()
    if(response && response.data)
    {
      setCaptchaData(response.data);
      setimageCaptcha("https://stage-admin1.aptaeguides.com" + response.data.image_url)
    }
  }

  useEffect(() => {
    loadCaptcha();
  }, []);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value,
    });
    if (!!errors[field])
      setErrors({
        ...errors,
        [field]: null,
      });
  };

  const findFormErrors = () => {
    const { id_email, captcha_1 } = form;
    const newErrors = {};
    if (!id_email) newErrors.id_email = "Please click the button.";
    if (!captcha_1 || captcha_1 === "")
      newErrors.captcha_1 = "Please enter captcha.";

    return newErrors;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = findFormErrors();
    if (!empty(newErrors)) {
      console.log(newErrors);
      setErrors(newErrors);
      setValidated(false);
    } else {
      setValidated(true);
    }
  };
  return (
    <Container
      className="resetContent min-vh-100 d-flex flex-column"
      fluid={true}
    >
      <Row className="loginFormContent">
        <Col xs={12} lg={12} md={12} sm={12}>
          <div className="card-title text-center">Reset Password</div>
          <div className="card-subtitle">
            <span className="text-center ">
              Only admin user can reset password through this page. If you are
              CMS user plaese contact admin to reset your password.
            </span>
          </div>
          <div className="resetPasswordmode">
            Select the Reset Password mode
          </div>
          <div className="loginFromContent">
            <Form validated={validated} onSubmit={handleSubmit}>
              <Form.Check
                type={"radio"}
                id={"id_email"}
                label={"Through Email"}
                required
                className="emailThrough"
                onChange={(e) => setField("id_email", e.target.value)}
                isInvalid={errors.id_email}
              />
              {imageCaptcha && imageCaptcha !== "" && (
                <InputGroup className="mb-4 captcha_img">
                  <InputGroup.Text id="basic-addon1">
                    <Image src={imageCaptcha} />
                  </InputGroup.Text>
                  <Form.Control
                    placeholder="Please enter captcha"
                    aria-label="Please enter captcha"
                    aria-describedby="basic-addon1"
                    type="text"
                    onChange={(e) => setField("captcha_1", e.target.value)}
                    isInvalid={errors.captcha_1}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.captcha_1}
                  </Form.Control.Feedback>
                </InputGroup>
              )}

              <Col xs={12} lg={12} md={12} sm={12}>
                <div
                  className="forgotPasswordLink"
                  onClick={() => {
                    navigate("/");
                  }}
                >
                  Back to Login page
                </div>
              </Col>

              <Button
                className="loginButton btn-info btn-lg btn-block"
                type="submit"
              >
                Submit
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default connect(null, null)(ResetPassword);
