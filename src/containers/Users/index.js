import React from "react";
import Layout from "../../components/layout";
import UsersListTable from "../../components/usersListTable";
import Breadcrumb from "../../components/breadcrumb";
import "./users.scss";
import { Row, Col, Button } from "react-bootstrap";


const Users = () => {
  return (
    <Layout
      tabActive="user"
      innerComponent={() => {
        return (
          <div className="usersContainer">
            <Row>
              <Col xs={12} md={8} lg={8}>
                <Breadcrumb
                  title={"Users"}
                  data={[
                    {
                      name: "Home",
                      link: "/dashboard",
                    },
                    {
                      name: "Users",
                      link: "",
                    },
                  ]}
                />
              </Col>
              <Col xs={12} md={4} lg={4} className="buttonContriner">
                <Button className="addButton btn-info btn-lg btn-block">
                  Add User
                </Button>
              </Col>
            </Row>

            <UsersListTable />
          </div>
        );
      }}
    />
  );
};

export default Users;
