import React from "react";
import Layout from "../../components/layout";
import ProfileFrom from "../../components/profileFrom";
import Breadcrumb from "../../components/breadcrumb";
import "./profile.scss";

const Profile = () => {
  return (
    <Layout
      tabActive="profile"
      innerComponent={() => {
        return (
          <div className="profileContainer">
            <Breadcrumb title={"Profile"} data={[
              {
                name : 'Home',
                link:'/dashboard'
              },
              {
                name : 'Profile',
                link:''
              }
            ]}/>
            <ProfileFrom />
          </div>
        );
      }}
    />
  );
};

export default Profile;
