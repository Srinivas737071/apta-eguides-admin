import ApiConfig from "../Api";


export const getCaptcha = async () => {
    try {
      const url = "/captcha/refresh/";
      return await ApiConfig.getData(url);
  
    } catch (err) {
      return err;
    }
  }


  export const userLogin = async (params) => {
    try {
      const data = new FormData();
      data.append("username", params.username);
      data.append("password", params.password);
      data.append("captcha_0", params.captcha_0);
      data.append("captcha_1", params.captcha_1);
      const url = "/accounts/api/login/";
      return await ApiConfig.postData(url,{},data);
    } catch (err) {
      return err;
    }
  }
  