import { LOGIN_STATUS_SUCCESS } from "../actions/types";

const initialState = {
  userLoginDeatils: {},
  loginStatus: false,
};

const userInfo = (state = initialState, action) => {
  const { type, payload } = action;
  if (typeof state === "undefined") {
    return state;
  }
  switch (type) {
    case LOGIN_STATUS_SUCCESS:
      return {
        ...state,
        userLoginDeatils: payload,
        loginStatus: true,
      };

    default:
      return state;
  }
};

export default userInfo;
