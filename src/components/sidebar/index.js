import React from "react";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from "react-pro-sidebar";

import "react-pro-sidebar/dist/css/styles.css";
import "./sidebar.scss";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { FaHome } from "react-icons/fa";

const Sidebar = (props) => {
  const { tabActive, menuCollapse } = props;
  const history = useNavigate();

  const onClickMenuTab = (url) => {
    history(url);
  };

  return (
    <>
      <div id="header">
        <ProSidebar collapsed={menuCollapse}>
          <SidebarHeader>
            <div className="logotext">
              <p>{menuCollapse ? "" : "e-Guides"}</p>
            </div>
          </SidebarHeader>
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem
                active={tabActive === "dashboard" ? true : false}
                icon={<FaHome />}
                onClick={() => {
                  if (tabActive !== "dashboard") {
                    onClickMenuTab("/dashboard");
                  }
                }}
              >
                Dashboard
              </MenuItem>
              <MenuItem
                active={tabActive === "profile" ? true : false}
                icon={<i className="fa fa-light fa-gear"></i>}
                onClick={() => {
                  if (tabActive !== "profile") {
                    onClickMenuTab("/profile");
                  }
                }}
              >
                Profile
              </MenuItem>
              <MenuItem
                active={tabActive === "user" ? true : false}
                icon={<i className="fa fa-light fa-user"></i>}
                onClick={() => {
                  if (tabActive !== "user") {
                    onClickMenuTab("/user");
                  }
                }}
              >
                User
              </MenuItem>
              <MenuItem
                active={tabActive === "places" ? true : false}
                icon={<i className="fa fa-solid fa-map"></i>}
                onClick={() => {
                  if (tabActive !== "places") {
                    onClickMenuTab("/places");
                  }
                }}
              >
                Places
              </MenuItem>

              <MenuItem
                active={tabActive === "image" ? true : false}
                icon={<i className="fa fa-thin fa-image"></i>}
                onClick={() => {
                  if (tabActive !== "image") {
                    onClickMenuTab("/image");
                  }
                }}
              >
                Image
              </MenuItem>
            </Menu>
          </SidebarContent>
          <SidebarFooter>
            <div className="logotext">
              <img src="./assets/images/apta_logo_new.png" alt="" />
            </div>
          </SidebarFooter>
        </ProSidebar>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  userLoginDeatils: state?.userInfo?.userLoginDeatils,
});

export default connect(mapStateToProps, null)(Sidebar);
