import React from "react";
import "./breadcrumb.scss";
import { useNavigate } from "react-router-dom";

const Breadcrumb = (props) => {
  const { title, data } = props;
  const history = useNavigate();

  return (
    <div className="breadcrumb-new">
      <h3 className="content-header-title mb-0 d-inline-block">{title}</h3>
      <div className="row breadcrumbs-top d-inline-block">
        <div className="breadcrumb-wrapper col-12">
          <ol className="breadcrumb">
            {data.map((item, i) => (
              <li
                className={
                  item.link === ""
                    ? "breadcrumb-item active"
                    : "breadcrumb-item linkabule"
                }
                key={i}
                onClick={() => {
                    if (item.link !== "") {
                        history(item.link);
                    }
                  }}
              >
                {item.name}
              </li>
            ))}
          </ol>
        </div>
      </div>
    </div>
  );
};

export default Breadcrumb;
