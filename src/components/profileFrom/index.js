import React from "react";
import { Form, Row, Col, Button } from "react-bootstrap";
import "./profileFrom.scss";

const ProfileFrom = () => {
  return (
    <div className="profileFrom">
      <Form>
        <Form.Group
          as={Row}
          className="mb-3"
          controlId="formPlaintextProfilename"
        >
          <Form.Label column md="2">
            <span className="red">*</span>
            Profile name
          </Form.Label>
          <Col md="4">
            <Form.Control type="text" placeholder="Profile name" required />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3" controlId="formPlaintextAddress">
          <Form.Label column md="2">
            <span className="red">*</span>
            Address
          </Form.Label>
          <Col md="4">
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Address"
              required
            />
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          className="mb-3"
          controlId="formPlaintextPrimaryEmail"
        >
          <Form.Label column md="2">
            <span className="red">*</span>
            Primary Email
          </Form.Label>
          <Col md="4">
            <Form.Control type="text" placeholder="Primary Email" required />
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          className="mb-3"
          controlId="formPlaintextSecondaryEmail"
        >
          <Form.Label column md="2">
            Secondary Email
          </Form.Label>
          <Col md="4">
            <Form.Control type="text" placeholder="Secondary Email" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} className="mb-3" controlId="formPrimaryPhone">
          <Form.Label column md="2">
            <span className="red">*</span>
            Primary Phone
          </Form.Label>
          <Col md="4">
            <Form.Control type="text" placeholder="Primary Phone" required />
          </Col>
        </Form.Group>

        <Form.Group
          as={Row}
          className="mb-3"
          controlId="formPlaintextSecondaryPhone"
        >
          <Form.Label column md="2">
            Secondary Phone
          </Form.Label>
          <Col md="4">
            <Form.Control type="text" placeholder="Secondary Phone" />
          </Col>
        </Form.Group>

        <div className="card-footer">
          <Button
            className="saveButton btn-info btn-lg btn-block"
            type="submit"
          >
            Save
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default ProfileFrom;
