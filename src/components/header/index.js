import React from "react";
import "./header.scss";
import { Nav, Navbar, NavDropdown, Container, Image } from "react-bootstrap";

const Header = (props) => {
  const { setMenuCollapse, menuCollapse } = props;
  const expand = false;
  return (
    <Navbar className="headerConatiner">
      <Container fluid>
        <Navbar.Toggle
          aria-controls={`offcanvasNavbar-expand-${expand}`}
          onClick={() => {
            setMenuCollapse(!menuCollapse);
          }}
        />
        <Nav>
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>Last login at: Mark Otto</Navbar.Text>
            <Navbar.Text>
              <span className="avatar avatar-online">
                <Image src="./assets/images/avatar.jpeg" alt="avatar" />
                <i></i>
              </span>
            </Navbar.Text>

            <Nav className="me-auto">
              <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                <NavDropdown.Item> Change Password</NavDropdown.Item>
                <NavDropdown.Item>Logout</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default Header;
