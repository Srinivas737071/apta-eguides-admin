import React, { useState } from "react";
import Sidebar from "../sidebar";
import Header from "../header";

import "./layout.scss";

const Layout = (props) => {
  const { tabActive, innerComponent } = props;
  const [menuCollapse, setMenuCollapse] = useState(false);

  return (
    <div className="layoutContainer">
      <div className="leftConatiner">
        <Sidebar tabActive={tabActive} menuCollapse={menuCollapse} />
      </div>
      <div className="rigthConatiner">
        <Header setMenuCollapse={setMenuCollapse} menuCollapse={menuCollapse} />
        {innerComponent()}
      </div>
    </div>
  );
};

export default Layout;
