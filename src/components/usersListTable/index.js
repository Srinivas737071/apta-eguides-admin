import React from "react";
import "./usersListTable.scss";
import { Table } from "antd";

const UsersListTable = () => {
  const columns = [
    {
      title: "#",
      dataIndex: "id",
      render: (value, item, index) => index + 1,
    },
    {
      title: "Name",
      dataIndex: "full_name",
      sorter: {
        compare: (a, b) => a.full_name - b.full_name,
        multiple: 2,
      },
    },
    {
      title: "UserName",
      dataIndex: "username",
      sorter: {
        compare: (a, b) => a.username - b.username,
        multiple: 1,
      },
    },

    {
      title: "Last Login",
      dataIndex: "last_login",
    },
    {
      title: "Is Active",
      dataIndex: "is_active",
      render: (text) => (text ? "Active" : "In Active"),
    },
    {
      title: "Edit",
      render: (value, item, index) => (
        <i
          className="fa fas fa-edit editIcon"
          onClick={() => {
            console.log(item, index);
          }}
        />
      ),
    },
  ];
  const data = [
    {
      first_name: "Test",
      full_name: "Test User",
      id: 2,
      is_active: true,
      last_login: "Thu, 10 Mar 2022, 12:18 PM",
      last_name: "User",
      username: "testuser",
    },
    {
      first_name: "Test1",
      full_name: "Test User1",
      id: 21,
      is_active: false,
      last_login: "Thu, 10 Mar 2022, 12:18 PM",
      last_name: "User",
      username: "testuser1",
    },
  ];
  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return <Table columns={columns} dataSource={data} onChange={onChange} />;
};

export default UsersListTable;
