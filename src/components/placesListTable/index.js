import React from "react";
import "./placesListTable.scss";
import { Table } from "antd";

const PlacesListTable = () => {
  const columns = [
    {
      title: "#",
      dataIndex: "id",
      render: (value, item, index) => index + 1,
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: {
        compare: (a, b) => a.name - b.name,
        multiple: 2,
      },
    },
    {
      title: "District",
      dataIndex: "city",
      sorter: {
        compare: (a, b) => a.city - b.city,
        multiple: 1,
      },
    },

    {
      title: "Status",
      dataIndex: "status",
    },
    {
      title: "Edit",
      render: (value, item, index) => (
        <i
          className="fa fas fa-edit editIcon"
          onClick={() => {
            console.log(item, index);
          }}
        />
      ),
    },
    {
      title: "Delete",
      render: (value, item, index) => (
        <i
          className="fa fa fa-trash deleteIcon"
          onClick={() => {
            console.log(item, index);
          }}
        />
      ),
    },
  ];
  const data = [
    {
      blueprint_url:
        "https://apta-eguides.s3-ap-south-1.amazonaws.com/blueprints/20-010aee65009b9faeb3a4e24ca777d3aaa51b0bd3.jpg",
      city: "Krishna",
      id: 20,
      is_published: false,
      is_qr_generated: true,
      latitude: "16.51027880",
      longitude: "80.64675290",
      name: "Bhavani Island",
      pincode: "521225",
      qr_code_url:
        "https://apta-eguides.s3-ap-south-1.amazonaws.com/qr-codes/pl-20.jpg",
      radius: 500,
      state: "Andhra Pradesh",
      status: "Draft",
      url: "http://www.hungama.com",
    },
    {
      blueprint_url:
        "https://apta-eguides.s3-ap-south-1.amazonaws.com/blueprints/19-a8fad3b45e0962efe91762868da596f79bc67343.jpg",
      city: "Kurnool",
      id: 19,
      is_published: true,
      is_qr_generated: true,
      latitude: "15.68405600",
      longitude: "78.17781900",
      name: "Rock Garden",
      pincode: "518010",
      qr_code_url:
        "https://apta-eguides.s3-ap-south-1.amazonaws.com/qr-codes/pl-19.jpg",
      radius: 500,
      state: "Andhra Pradesh",
      status: "Published",
      url: "google.com",
    },
  ];
  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };

  return <Table columns={columns} dataSource={data} onChange={onChange} />;
};

export default PlacesListTable;
